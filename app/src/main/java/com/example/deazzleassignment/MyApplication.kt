package com.example.deazzleassignment

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.example.deazzleassignment.baseUtils.AppDetails

class MyApplication:Application(),Application.ActivityLifecycleCallbacks {
    override fun onCreate() {
        super.onCreate()
        AppDetails.context = applicationContext
    }
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityResumed(activity: Activity) {
    }

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }
}