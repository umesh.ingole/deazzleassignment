package com.example.deazzleassignment.baseActivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.deazzleassignment.R
import com.example.deazzleassignment.baseUtils.AppDetails
import com.example.deazzleassignment.databinding.ActivityMainBinding
import com.example.deazzleassignment.tinder.MainActivityViewModel
import com.example.deazzleassignment.tinder.ProfileAdapter


class MainActivity : AppCompatActivity() {
    lateinit var activityMainBinding: ActivityMainBinding
    lateinit var mainActivityViewModel: MainActivityViewModel
    lateinit var profileAdapter: ProfileAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        AppDetails.context = this

        /*activityMainBinding.apply {
            userImage = "https://androidwave.com/wp-content/uploads/2019/01/profile_pic.jpg"
        }*/

        /*  activityMainBinding.swipeView.getBuilder<SwipePlaceHolderView, SwipeViewBuilder<SwipePlaceHolderView>>()
              .setDisplayViewCount(3)
              .setSwipeDecor(SwipeDecor()
                  .setPaddingTop(20)
                  .setRelativeScale(0.01f)
                  .setSwipeInMsgLayoutId(R.layout.tinder_swipe_in_msg_view)
                  .setSwipeOutMsgLayoutId(R.layout.tinder_swipe_out_msg_view))*/


        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        mainActivityViewModel.getData()

        profileAdapter = ProfileAdapter(this, emptyList()) { profile, status ->
            var updateProfile = profile.also {
                if (status) {
                    it.status = 1
                   // it.accept = "Member Accepted"
                    //it.reject = "Reject"
                } else {
                    it.status = 2
                   // it.accept = "Accept"
                  //  it.reject = "Member Rejected"
                }
            }
            mainActivityViewModel.update(updateProfile)
        }

        activityMainBinding.profileRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = profileAdapter
        }

        mainActivityViewModel.getProfiles().observe(this, Observer {
            profileAdapter.setData(it)
        })

        /*activityMainBinding.rejectBtn.setOnClickListener {
            activityMainBinding.swipeView.doSwipe(false)
        }
        activityMainBinding.acceptBtn.setOnClickListener {
            activityMainBinding.swipeView.doSwipe(true)
        }*/

    }
}