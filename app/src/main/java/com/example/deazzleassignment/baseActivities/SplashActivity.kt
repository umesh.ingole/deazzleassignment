package com.example.deazzleassignment.baseActivities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.example.deazzleassignment.R
import com.example.deazzleassignment.baseUtils.AppSharePreference


class SplashActivity : AppCompatActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            //Log.e("FCM", AppSharePreference.fcmToken!!)
            val intent = Intent(applicationContext, MainActivity::class.java)
            //val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash_screen)
        //returnMid()
        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
    }

    public override fun onDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }

    fun returnMid() {
        val arr = intArrayOf(20, 31, 14, 40, 26, 48, 64, 10, 87, 24)
        val iterationArray: ArrayList<Int>? = ArrayList()
        var temp: Int? = null
        var flage:Boolean=false
        /* var mid : Int?=0
         var temp : Int?=0
         var r=arr.size
         var l=0
         mid = l + (4-1)/2
         Log.d("Mid == " ,r.toString() +" "+mid.toString())
        return mid*/
        for (i in 0 until arr.size) {
            for (j in i + 1 until arr.size) {

                if (arr[i] > arr[j]) {
                    temp = arr[i]
                    arr[i] = arr[j]
                    arr[j] = temp
                    flage = true
                }
                if(flage){
                    iterationArray!!.add(arr[j])
                }
                flage = true
            }
            Log.d("Iteration == ", iterationArray.toString())
            iterationArray!!.clear()
        }
        for(i:Int in 0 until arr.size){
            Log.d("After Sorting == ", arr[i].toString())
        }
    }
}

