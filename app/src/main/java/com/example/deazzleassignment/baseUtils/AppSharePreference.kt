package com.example.deazzleassignment.baseUtils


class AppSharePreference:ConfigureSharePreference(){
   companion object{
       private val key_fcm_token = "keyFcmToken"
       private val key_user_name = "keyUserName"
       private val key_is_profile_data_present = "KeyIsProfileDataPresent"

       var fcmToken:String?
       get() = getString(key_fcm_token,null)
       set(value) = setString(key_fcm_token,value!!)

       var username:String?
       get() = getString(key_user_name,null)
       set(value) = setString(key_user_name,value!!)

       var isProfileDataPresent:Boolean?
       get() = getBoolean(key_is_profile_data_present,false)
       set(value) = setBoolean(key_is_profile_data_present,value!!)
   }





}