package com.example.deazzleassignment.baseUtils

import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys

open class ConfigureSharePreference {

    companion object {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
       /*val mainKey = MasterKey.Builder(AppDetails.context!!)
           .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
           .build()*/

        val sharedPreferences = EncryptedSharedPreferences.create(
            "MyApplicationSharedPreference",
            masterKeyAlias,
            AppDetails.context!!,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        private val editor: SharedPreferences.Editor
            get() = sharedPreferences!!.edit()

        open fun setString(keyValue: String, value: String) {
            val editor = editor
            editor.putString(keyValue, value)
            editor.commit()
        }

        open fun getString(key: String, defValue: String?): String? {
            val pref = sharedPreferences
            return pref.getString(key, defValue)
        }

        open fun setBoolean(keyValue:String,value:Boolean?){
            val editor = editor
            editor.putBoolean(keyValue, value!!)
            editor.commit()
        }

        open fun getBoolean(key:String,dafValue:Boolean?):Boolean{
            val pref = sharedPreferences
            return pref.getBoolean(key,dafValue!!)
        }
/*
  fun setLoginObject(keyValue: String, value: LoginResponse) {
        val editor = editor
        val gson = Gson()
        val data = gson.toJson(value, LoginResponse::class.java)
        editor.putString(keyValue, data)
        editor.commit()
    }

    fun getLoginObject(keyValue: String, defaultValue: String?): LoginResponse {
        val sharedPreferences = preferences
        val data = sharedPreferences!!.getString(keyValue, defaultValue)
        val gson = Gson()
        return gson.fromJson(data, LoginResponse::class.java)
    }*/
    }
}