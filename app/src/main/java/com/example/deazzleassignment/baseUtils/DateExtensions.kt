package com.example.deazzleassignment.baseUtils

import java.text.SimpleDateFormat
import java.util.*

fun String.getDefaultDate(): String {
    var sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    var date = sdf.parse(this)
    return sdf.format(date)
}