package com.example.deazzleassignment.network

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface  ApiInterface {

    @GET("api/?results=10")
    fun getDNAData(): Call<JsonObject>

    @Multipart
    @POST("updateUserImage")
    fun updateUserImage(
        @Part("user_id") user_id: RequestBody,
        @Part profile_image: MultipartBody.Part
    ): Call<JsonObject>

}
