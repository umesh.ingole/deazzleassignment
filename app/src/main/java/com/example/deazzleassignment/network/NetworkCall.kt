package com.example.deazzleassignment.network
import android.content.ContentValues
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.Log
import com.example.deazzleassignment.baseUtils.AppDetails
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NetworkCall(val repoView : ApiCallType.ViewData) : ApiCallType.call{
   private var progressDialog : CustomProgressDialog?=null
    override fun onClick(
        caseConstants: ApiConstants,
        para: Array<String>,
        context: Context,
        showProgressBar: Boolean?
    ) {
        if (showProgressBar!!) {
            initAndShowProgressBar(context)
        }
        val retrofit = ApiClient.client
        val requestInterface = retrofit.create(ApiInterface::class.java)
        val accessTokenCall: Call<JsonObject>
        when (caseConstants) {
            /* --------------- Umesh --------------------- */
            ApiConstants.GETDATA -> {
                accessTokenCall = requestInterface.getDNAData()
                callApiWithAccessToken(accessTokenCall, context, ApiConstants.GETDATA)
            }
        }
    }

    private fun callApiWithAccessToken(accessTokenCall: Call<JsonObject>, context: Context, apiConstants: ApiConstants) {
        accessTokenCall.enqueue(object :Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (progressDialog != null && progressDialog!!.isShowing)
                    try {
                        if (progressDialog!!.isShowing)
                            progressDialog!!.dismiss()
                    } catch (e: Exception) {
                        Log.e(ContentValues.TAG, "onResponse: $e")
                    }

                if(response.body()!=null){
                   repoView.setViewData(response.body().toString(),apiConstants)
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }
        })
    }

    private fun initAndShowProgressBar(context: Context) {
        progressDialog = CustomProgressDialog(AppDetails.context, "loading");
            progressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(0))
        progressDialog!!.isIndeterminate = true
        progressDialog!!.setCancelable(false)
        try {
            progressDialog!!.show()
        } catch (e: Exception) {
            Log.e(ContentValues.TAG, "initAndShowProgressBar: $e")
        }
    }
}