package com.example.deazzleassignment.room

import androidx.room.TypeConverter
import com.example.deazzleassignment.tinder.Dob
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class ConvertersDob {
        @TypeConverter
        fun fromString(value: String?): Dob {
            val objType: Type = object : TypeToken<Dob?>() {}.type
            return Gson().fromJson<Dob>(value, objType)
        }

        @TypeConverter
        fun objToString(list: Dob?): String {
            val gson = Gson()
            return gson.toJson(list)
        }
}
