package com.example.deazzleassignment.room

import androidx.room.TypeConverter
import com.example.deazzleassignment.tinder.Location
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class ConvertersLocation {
        @TypeConverter
        fun fromString(value: String?): Location {
            val objType: Type = object : TypeToken<Location?>() {}.type
            return Gson().fromJson<Location>(value, objType)
        }

        @TypeConverter
        fun objToString(list: Location?): String {
            val gson = Gson()
            return gson.toJson(list)
        }
}
