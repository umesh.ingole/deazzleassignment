package com.example.deazzleassignment.room

import androidx.room.TypeConverter
import com.example.deazzleassignment.tinder.Name
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class ConvertersName {
        @TypeConverter
        fun fromString(value: String?): Name {
            val objType: Type = object : TypeToken<Name?>() {}.type
            return Gson().fromJson<Name>(value, objType)
        }

        @TypeConverter
        fun objToString(list: Name?): String {
            val gson = Gson()
            return gson.toJson(list)
        }
}
