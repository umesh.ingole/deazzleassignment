package com.example.deazzleassignment.room

import androidx.room.TypeConverter
import com.example.deazzleassignment.tinder.Picture
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class ConvertersPicture {
        @TypeConverter
        fun fromString(value: String?): Picture {
            val objType: Type = object : TypeToken<Picture?>() {}.type
            return Gson().fromJson<Picture>(value, objType)
        }

        @TypeConverter
        fun objToString(list: Picture?): String {
            val gson = Gson()
            return gson.toJson(list)
        }
}
