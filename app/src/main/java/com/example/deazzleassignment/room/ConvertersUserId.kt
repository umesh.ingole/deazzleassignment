package com.example.deazzleassignment.room

import androidx.room.TypeConverter
import com.example.deazzleassignment.tinder.UserId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class ConvertersUserId {
    var data = UserId("","")

        @TypeConverter
        fun fromString(value: String?): UserId?{
            /*try {*/
                value?.let {
                    val objType: Type = object : TypeToken<UserId?>() {}.type
                    data = Gson().fromJson<UserId?>(value, objType)
                }
           /* }catch (e:Exception){

            }*/
            return data
        }

        @TypeConverter
        fun objToString(list: UserId?): String {
            var str=""
            /*list?.let {*/
                val gson = Gson()

                str = gson.toJson(list)
           /* }*/
            return str
        }
}
