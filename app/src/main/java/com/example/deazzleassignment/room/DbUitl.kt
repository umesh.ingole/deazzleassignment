package com.example.deazzleassignment.room

import com.example.deazzleassignment.tinder.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun rePopulateDb(database: MyDataBase?) {
    database?.let { db ->
        withContext(Dispatchers.IO) {
            val profileDao: ProfileDao = db.profileDao()
            profileDao.deleteAll()
            var profile = Profile(
                0, Name("Mr", "Umesh", "Ingole"),
                Dob("12-10-2005", "15"),
                Location(Street(5, "High Street"), "Akola", "Maharashtra", "India", "444001"),
                Picture(null, "https://randomuser.me/api/portraits/med/men/11.jpg", null),
                UserId("FA","12343452"),0
            )
            profileDao.insert(profile)
        }
    }
}

suspend fun insertAllData(database: MyDataBase?, resultModel: List<Profile?>?) {
    database.let {
        withContext(Dispatchers.IO) {
            var dao = it!!.profileDao()
            dao.deleteAll()
            dao.insertPosts(resultModel)
        }
    }
}

