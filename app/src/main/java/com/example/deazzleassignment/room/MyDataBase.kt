package com.example.deazzleassignment.room

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.deazzleassignment.tinder.Profile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@Database(entities = [Profile::class],version = 1)
@TypeConverters(*[ConvertersName::class,ConvertersPicture::class,ConvertersLocation::class,ConvertersDob::class,ConvertersUserId::class])
abstract class MyDataBase : RoomDatabase() {
    abstract fun profileDao():ProfileDao
    companion object {
        @Volatile
        private var INSTANCE: MyDataBase? = null

        fun getDataBase(context: Context): MyDataBase {
            return INSTANCE ?: synchronized(this) {
                val inctance = Room.databaseBuilder(
                    context.applicationContext, MyDataBase::class.java,
                    "profile"
                ).addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Log.d("MoviesDatabase", "populating with data...")
                        GlobalScope.launch(Dispatchers.IO) { rePopulateDb(INSTANCE) }
                    }
                }).build()
                INSTANCE = inctance
                inctance
            }
        }
    }
}