package com.example.deazzleassignment.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.deazzleassignment.tinder.Profile


@Dao
interface ProfileDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(doc: Profile?)

    @Query("SELECT * from Profile ORDER BY id ASC")
    fun getAllPosts(): LiveData<List<Profile>>

    @Query("DELETE FROM Profile")
    fun deleteAll()


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(docs: List<Profile?>?)

    @Update()
    fun update(profile: Profile?)

}