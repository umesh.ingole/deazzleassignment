package com.example.deazzleassignment.tinder

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.deazzleassignment.room.MyDataBase
import kotlinx.coroutines.*

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    lateinit var profils: LiveData<List<Profile>>
    var profileRepo: ProfileRepo = ProfileRepo(application)
    var db: MyDataBase = MyDataBase.getDataBase(application)

    fun getProfiles(): LiveData<List<Profile>> {
        profils = profileRepo.getDataFromDB()
        return profils
    }

    fun getData() {
        profileRepo.getData()
    }

    fun update(profile: Profile) {
        viewModelScope.launch {
            CoroutineScope(Dispatchers.IO).launch{
                db.let {
                    var dao = it.profileDao()
                    dao.update(profile)
                }
            }
        }
    }
    

}