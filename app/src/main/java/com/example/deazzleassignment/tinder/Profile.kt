package com.example.deazzleassignment.tinder

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.deazzleassignment.baseUtils.getDefaultDate
import com.example.deazzleassignment.room.*
import com.fasterxml.jackson.annotation.JsonInclude
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Example {
    @SerializedName("results")
    @Expose
    var profiles: List<Profile>? = null
}

@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(tableName = "Profile")
data class Profile(
    @PrimaryKey(autoGenerate = true)
    var pid: Int,

    @SerializedName("name")
    @TypeConverters(ConvertersName::class)
    val name: Name,

    @SerializedName("dob")
    @TypeConverters(ConvertersDob::class)
    val dob: Dob,

    @SerializedName("location")
    @TypeConverters(ConvertersLocation::class)
    val address: Location,

    @SerializedName("picture")
    @TypeConverters(ConvertersPicture::class)
    var profileImage: Picture,

    @SerializedName("id")
    @TypeConverters(ConvertersUserId::class)
    var id: UserId,

    @SerializedName("status")
    var status:Int=0

 /*   var accept:String,
    var reject:String,
*/

) : Serializable {
    companion object {
        @JvmStatic // add this line !!
        @BindingAdapter("userImage")
        fun loadImage(view: ImageView, imageUrl: String?) {
            Glide.with(view.context)
                .load(imageUrl).apply(RequestOptions().fitCenter())
                .into(view)
        }
    }


}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Dob(
    @SerializedName("date")
    @Expose
    val date: String,
    @SerializedName("age")
    @Expose
    val age: String
) : Serializable {
    fun getdate(): String {
        return date.getDefaultDate()
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserId(
    @SerializedName("name")
    @Expose
    val name: String?,
    @SerializedName("value")
    @Expose
    val value: String?
) : Serializable {
    override fun toString(): String {
        return "$name $value"
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Location(
    @SerializedName("street")
    @Expose
    var street: Street? = null,

    @SerializedName("city")
    @Expose
    var city: String? = null,

    @SerializedName("state")
    @Expose
    var state: String? = null,

    @SerializedName("country")
    @Expose
    var country: String? = null,

    @SerializedName("postcode")
    @Expose
    var postcode: String? = null,
) : Serializable {
    override fun toString(): String {
        return "$street, $city, $state, $country, $postcode"
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Street(
    @SerializedName("number")
    @Expose
    var number: Int? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null
) : Serializable {
    override fun toString(): String {
        return "$number $name"
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Name(
    @SerializedName("title")
    @Expose
    var title: String? = null,

    @SerializedName("first")
    @Expose
    var first: String? = null,

    @SerializedName("last")
    @Expose
    var last: String? = null
) : Serializable {
    override fun toString(): String {
        return "$title $first $last"
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Picture(
    @SerializedName("large")
    @Expose
    var large: String? = null,

    @SerializedName("medium")
    @Expose
    var medium: String? = null,

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
) : Serializable

