package com.example.deazzleassignment.tinder

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.deazzleassignment.R
import com.example.deazzleassignment.databinding.TinderCardViewBinding
import kotlinx.android.synthetic.main.tinder_card_view.view.*

class ProfileAdapter(
    var context: Context,
    var list: List<Profile>?,
    val onClick: (obj: Profile, flag: Boolean) -> Unit
) :
    RecyclerView.Adapter<ProfileAdapter.ProfileViewModel>() {
    lateinit var tinderCardViewBinding: TinderCardViewBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewModel {
        tinderCardViewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.tinder_card_view,
            parent, false
        )
        return ProfileViewModel(tinderCardViewBinding)
    }

    override fun onBindViewHolder(holder: ProfileViewModel, position: Int) {

        var profile = list?.get(position)
        holder.onBind(profile)

    }

    override fun getItemCount(): Int {
        return list!!.size
    }
/*

     override fun getItemId(position: Int): Long {
         return position.toLong()
     }
*/

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ProfileViewModel(tinderCardViewBinding: TinderCardViewBinding) :
        RecyclerView.ViewHolder(tinderCardViewBinding.root) {
        var acceptBtn = tinderCardViewBinding.root.acceptBtn
        var rejectBtn = tinderCardViewBinding.root.rejectBtn

        fun onBind(profile: Profile?) {
            tinderCardViewBinding.profileImage = profile!!.profileImage.large
            tinderCardViewBinding.profileObj = profile

            acceptBtn.setOnClickListener {
                onClick(list?.get(position)!!, true)

            }
            rejectBtn.setOnClickListener {
                onClick(list?.get(position)!!, false)

            }

            when (profile!!.status) {
                1 -> {
                   /* acceptBtn.text =
                    rejectBtn.text =
                    acceptBtn.setBackgroundColor(context.getColor(R.color.red))
                    rejectBtn.setBackgroundColor(context.getColor(R.color.green))*/
                    setViewText("Member accepted","Reject",context.getColor(R.color.red),context.getColor(R.color.green))
                }
                2 -> {
                    /*rejectBtn.text = "Member Decline"
                    acceptBtn.text = "Accept"
                    rejectBtn.setBackgroundColor(context.getColor(R.color.red))
                    acceptBtn.setBackgroundColor(context.getColor(R.color.green))*/
                    setViewText("Accept","Member Decline",context.getColor(R.color.green),context.getColor(R.color.red))

                }
                else -> {
                    /*acceptBtn.text = "Accept"
                    rejectBtn.text = "Reject"
                    acceptBtn.setBackgroundColor(context.getColor(R.color.green))
                    rejectBtn.setBackgroundColor(context.getColor(R.color.green))*/
                    setViewText("Accept","Reject",context.getColor(R.color.green),context.getColor(R.color.green))

                }
            }

        }

        private fun setViewText(accText: String, rejText: String, accColor: Int, rejColor: Int){
            acceptBtn.text = accText
            rejectBtn.text = rejText
            acceptBtn.setBackgroundColor(accColor)
            rejectBtn.setBackgroundColor(rejColor)
        }

    }

    fun setData(updatedList: List<Profile>) {
        this.list = updatedList
        notifyDataSetChanged()
    }
}