package com.example.deazzleassignment.tinder

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.deazzleassignment.baseUtils.AppSharePreference
import com.example.deazzleassignment.network.ApiCallType
import com.example.deazzleassignment.network.ApiConstants
import com.example.deazzleassignment.network.NetworkCall
import com.example.deazzleassignment.room.MyDataBase
import com.example.deazzleassignment.room.ProfileDao
import com.example.deazzleassignment.room.insertAllData
import com.google.gson.Gson
import kotlinx.coroutines.*


class ProfileRepo(var application: Application): ApiCallType.ViewData {
    var db: MyDataBase = MyDataBase.getDataBase(application)
    var mutableProfile:MutableLiveData<List<Profile>>
    var liveDataList:LiveData<List<Profile>>
    private var api: ApiCallType.call? = null
    private var profileDao: ProfileDao? = null

    init {
        api = NetworkCall(this)
        profileDao = db.profileDao()
        liveDataList = profileDao!!.getAllPosts()
        mutableProfile = MutableLiveData()
    }

    fun getData(){
        if(AppSharePreference.isProfileDataPresent!!){
            getDataFromDB()
        }else{
            getDataFromAPI()
        }
    }

    private fun getDataFromAPI() {
       /* GlobalScope.launch(Dispatchers.IO) {
        }*/
        api!!.onClick(ApiConstants.GETDATA, arrayOf(""),application,true)

    }

    fun getDataFromDB(): LiveData<List<Profile>>{
        return liveDataList
    }


   /* fun getProfilesData(): LiveData<List<Profile>> {
        profiles = mutableProfile
        return profiles
    }*/

    override fun setViewData(str: String, apiConstants: ApiConstants) {
        var resp = Gson().fromJson(str, Example::class.java)
        /*resp.profiles.docs.forEach {
            var itr = it.abstractName.listIterator()
            while (itr.hasNext()) {
                itr.set(itr.next().replace("\n", ""))
            }
        }*/
        AppSharePreference.isProfileDataPresent = true
        // set data in database using coroutine
        /*GlobalScope.launch(Dispatchers.IO) {
            insertAllData(db, resp.profiles)
        }*/
        CoroutineScope(Dispatchers.IO).launch {
            insertAllData(db, resp.profiles)
        }
    }



}